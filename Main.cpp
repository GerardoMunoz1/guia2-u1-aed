#include <iostream>
#include "Pila.h"

using namespace std;

int main(){

    int x, y;

    cout << "Limite X: ";
    cin >> x;

    Pila pila = Pila(x);

    pila.nuevo_contenedor();
    pila.nuevo_contenedor();
    pila.nuevo_contenedor();
    pila.imprimir_pila();
    return 0;
}
