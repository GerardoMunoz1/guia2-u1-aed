#include <iostream>
#include <stack>
#include "Contenedor.h"

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{

    private:
        int limite; // x
        int tope;   // y

    public:
        Contenedor *contenedores;

        Pila(int limite);
        bool pila_llena();
        bool pila_vacia();
        void push(Contenedor *contenedor);
        void nuevo_contenedor();
        void top();
        int buscar_contenedor(string empresa, int numero);
        Contenedor get_contenedores();
        void imprimir_pila();
};
#endif
