#include <iostream>
#include <stack>
#include <vector>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;


Pila::Pila(int limite){

    this->limite = limite;  // y-máx
    this->tope = 0;      // y-actual
}

bool Pila::pila_vacia(){

    bool vacia = 0;

    if(this->tope == 0){
        vacia = 1;
    }

    return vacia;
}


bool Pila::pila_llena(){

    bool llena = 0;

    if(this->tope == this->limite){
        llena = 1;
    }

    return llena;
}

void Pila::push(Contenedor *contenedor){

    if (this->pila_vacia() == 1){
        this->contenedores = contenedor;
        this->tope++;

    }
    else if(this->pila_llena() == 1){
        cout << "Pila llena, llegamos al tope." << endl;
    }
    else{
        int contenedor_actual = this->tope;
        Contenedor *objeto;

        for (int z = 0; z < contenedor_actual; z++) {

            if (z == 0){
                objeto = this->contenedores;
            }
            else{
                objeto = objeto->contenedor_sig;
            }
        }
        objeto->contenedor_sig = contenedor;
        this->tope++;
    }
}

void Pila::nuevo_contenedor(){

    string empresa;
    int numero;

    cout << "EMPRESA: ";
    cin >> empresa;

    cout << "\nNUMERO: ";
    cin >> numero;

    Contenedor *nuevo = new Contenedor(empresa, numero);
    this->push(nuevo);

}

void Pila::top(){

    int contenedor_actual = this->tope;
    Contenedor *objeto;

    for (int z = 0; z < contenedor_actual-1; z++) {

        if (z == 0){
            objeto = this->contenedores;
        }
        else{
            objeto = objeto->contenedor_sig;
        }
    }

    objeto->contenedor_sig = (Contenedor *) NULL;
}

int Pila::buscar_contenedor(string empresa, int numero){

    int posicion = 0;

    for(int i=0; i<this->tope; i++){

        if(this->contenedores[i].get_empresa() == empresa && this->contenedores[i].get_numero() == numero){
            posicion = i+1;
            break;
        }
    }

    return posicion;
}

void Pila::imprimir_pila(){

    int contenedor_actual = this->tope;
    Contenedor *objeto;
    vector<string> imprimir;

    for (int z = 0; z < contenedor_actual; z++) {

        if (z == 0){
            objeto = this->contenedores;
        }
        else{
            objeto = objeto->contenedor_sig;
        }

        string string_actual = "|" + to_string(objeto->get_numero()) + " - " + objeto->get_empresa() + "\t|";
        imprimir.push_back(string_actual);
    }

    for (int a = contenedor_actual-1; a >= 0; a--) {
        cout << imprimir[a] << endl;
    }

}
